<?php

function toCamelCase(string $str)
{
    $parts = explode('-', $str);
    $parts = array_map('ucfirst', $parts);
    return lcfirst(implode('', $parts));
}

function handleSqlErrors($query, $error_message)
{
    echo '<pre>';
    echo $query;
    echo '</pre>';
    echo $error_message;
}
