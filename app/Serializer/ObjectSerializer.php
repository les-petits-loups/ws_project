<?php

namespace App\Serializer;

use ReflectionClass;

class ObjectSerializer
{
    public function toJson(object $entity, bool $stringify = true)
    {
        $data = [];

        $reflectionClass = new ReflectionClass($entity);
        $properties = $reflectionClass->getProperties();

        foreach ($properties as $property) {
            $parts = explode('_', $property->getName());
            $parts = array_map('ucfirst', $parts);
            $propertyName = implode('', $parts);

            $getterName = 'get' . $propertyName;

            if (method_exists($entity, $getterName)) {
                $value = $entity->$getterName();
            } else {
                $property->setAccessible(true);
                $value = $property->getValue();
            }

            $data[$property->getName()] = $value;
        }

        return $stringify ? json_encode($data, JSON_PRETTY_PRINT) : $data;
    }

    function toXml($data, &$xml_data) {
        foreach($data as $key => $value) {
            if ($key !== "id") {
                if(is_array($value)) {
                    if(is_numeric($key)) {
                        $key = 'item'.$key; //dealing with <0/>..<n/> issues
                    }
                    $subnode = $xml_data->addChild($key);
                    if (!empty($data['user'])) {
                        $subnode->addAttribute('id', $data['user']['id']);
                    }
                    $this->toXml($value, $subnode);
                } else {
                    $xml_data->addChild("$key",htmlspecialchars("$value"));
                }
            }
        }
    }
    
}
