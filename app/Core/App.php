<?php

namespace App\Core;

use App\Core\Controller\AbstractController;
use App\Core\Routing\Route;
use FastRoute\Dispatcher;
use function FastRoute\simpleDispatcher;
use FastRoute\RouteCollector;
use App\Database\LoginDB;
use Psr\Http\Message\ServerRequestInterface;

class App
{
    protected $rootDir = __DIR__ . '/../..';

    public function handle(ServerRequestInterface $request)
    {
        echo $this->disptach($request);
    }

    protected function disptach(ServerRequestInterface $request): string
    {
        /** @var Route[] $routes */
        $routes = require($this->rootDir . '/config/routing.php');

        $dispatcher = simpleDispatcher(function (RouteCollector $r) use ($routes) {
            foreach ($routes as $route) {
                $r->addRoute(
                    $route->getMethod(),
                    $route->getPath(),
                    $route->getAction()
                );
            }
        });

        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = str_replace('/index.php', '', $_SERVER['REQUEST_URI']);
        $uri = !empty($uri) ? $uri : '/';

        $pos = strpos($uri, '?');
        if (false !== $pos) {
            $uri = substr($uri, 0, $pos);
        }
        $uri = rawurldecode($uri);

        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                return call_user_func_array(new \App\Action\Errors\_404(), []);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                return call_user_func_array(new \App\Action\Errors\_401(), []);
                break;
            case Dispatcher::FOUND:
                [, $action, $vars] = $routeInfo;

                $isLogged = $this->isLogged();

                if (!class_exists($action)) {
                    throw new \RuntimeException('Class ' . $action . ' does not exists!');
                } elseif ($action !== 'App\Action\Login\Login' 
                            && $action !== 'App\Action\Login\Redirect' 
                            && $action !== 'App\Action\Errors\_AnonymousError' 
                            && !$isLogged
                            && substr($action, 0, 7) !== 'App\Api') {
                    return call_user_func_array(new \App\Action\Login\Login(), $vars);
                } elseif ($action === 'App\Action\Login\Login' && $isLogged) {
                    return call_user_func_array(new \App\Action\Home(), $vars);
                }

                $instance = new $action();

                if (!$instance instanceof AbstractController) {
                    throw new \LogicException('Action must be an instance of AbstractController');
                }

                $instance->setRequest($request);

                return call_user_func_array($instance, $vars);
                break;
        }
    }

    private function isLogged(): bool
    {
        $isLogged = false;

        // First check with session if admin is logged
        if (!empty($_SESSION['logged'])) {
            $isLogged = true;
            return $isLogged;
        }

        // Second check with cookies & token if admin is logged
        if (!empty($_COOKIE['admin_id']) && !empty($_COOKIE['token']) && !empty($_COOKIE['selector'])) {
            $isTokenValid = false;
            $isSelectorValid = false;
            $isDateValid = false;

            $currentTime = time();
            $currentDate = date("Y-m-d H:i:s", $currentTime);

            $loginDB = new LoginDB();
            $adminToken = $loginDB->getTokenByAdminID((int)$_COOKIE['admin_id']);

            if (!empty($adminToken["id"])) {
                if (password_verify($_COOKIE["token"], $adminToken["token"])) {
                    $isTokenValid = true;
                }

                if (password_verify($_COOKIE["selector"], $adminToken["selector"])) {
                    $isSelectorValid = true;
                }

                if ($adminToken["expires"] >= $currentDate) {
                    $isDateValid = true;
                }

                if ($isTokenValid && $isSelectorValid && $isDateValid) {
                    $isLogged = true;
                    return $isLogged;
                } else {
                    if (!empty($adminToken["id"])) {
                        $delete = $loginDB->deleteToken($adminToken["id"]);

                        if ($delete === false) {
                            header("Location: /error");
                        }
                    }

                    $loginDB->clearCookies();
                }
            }
        }
        return $isLogged;
    }
}
