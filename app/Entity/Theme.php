<?php

namespace App\Entity;

class Theme implements \JsonSerializable
{
    private ?int $id = null;
    private string $name;

    public function __construct(array $props = [])
    {
        foreach ($props as $prop => $value) {
            $setter = 'set' . ucfirst(toCamelCase($prop));

            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }

    public function jsonSerialize()
    {
        $array['id'] = $this->getId();
        $array['name'] = $this->getName();
        return $array;
    }

    // --- GETTERS ---
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    // --- SETTERS ---
    public function setId(int $id): Theme
    {
        $this->id = $id;
        return $this;
    }

    public function setName(string $name): Theme
    {
        $this->name = $name;
        return $this;
    }


}
