<?php
namespace App\Entity; 

class PossibleAnswer
{
  private ?int $id = null;
  private ?Question $question = null;
  private ?string $label = null;
  private ?bool $isRight = null;

  public function __construct(array $props = [])
  {
    foreach ($props as $prop => $value) {
      $setter = 'set' . ucfirst(toCamelCase($prop));

      if (method_exists($this, $setter)) {
        $this->$setter($value);
      }
    }
  }

  // --- GETTERS ---
  public function getId(): ?int { return $this->id; }
  public function getLabel(): string { return $this->label; }
  public function getQuestion(): Question { return $this->question; }
  public function getIsRight(): bool { return $this->isRight; }

  // --- SETTERS ---
  public function setId(int $id): PossibleAnswer
  {
    $this->id = $id;
    return $this;
  }

  public function setQuestion(Question $question): PossibleAnswer
  {
    $this->question = $question;
    return $this;
  }

  public function setLabel(string $label): PossibleAnswer
  {
    $this->label = $label;
    return $this;
  }

  public function setIsRight(bool $isRight): PossibleAnswer
  {
    $this->isRight = $isRight;
    return $this;
  }
}
