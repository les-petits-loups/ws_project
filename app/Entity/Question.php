<?php
namespace App\Entity; 

class Question
{
  use HydrationTrait; 
  
  private ?int $id = null;
  private ?string $label = null;
  private ?Theme $theme = null;

  public function __construct(array $props = [])
  {
    foreach ($props as $prop => $value) {
      $setter = 'set' . ucfirst(toCamelCase($prop));

      if (method_exists($this, $setter)) {
        $this->$setter($value);
      }
    }
  }

  // --- GETTERS ---
  public function getId(): ?int { return $this->id; }
  public function getLabel(): ?string { return $this->label; }
  public function getTheme(): ?Theme { return $this->theme; }

  // --- SETTERS ---
  public function setId(int $id): Question
  {
    $this->id = $id;
    return $this;
  }

  public function setLabel(string $label): Question
  {
    $this->label = $label;
    return $this;
  }

  public function setTheme(Theme $theme): Question
  {
    $this->theme = $theme;
    return $this;
  }
}
