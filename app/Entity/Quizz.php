<?php
namespace App\Entity; 

class Quizz
{
  use HydrationTrait;

  private ?int $id = null;
  private int $mode;
  private ?array $questions = null;
  private User $user1;
  private ?User $user2 = null; // Optional
  private \DateTime $startAt;
  private ?User $winner = null; // Optional
  
  public function __construct(array $props = [])
  {
    foreach ($props as $prop => $value) {
      $setter = 'set' . ucfirst(toCamelCase($prop));

      if (method_exists($this, $setter)) {
        $this->$setter($value);
      } else {
        var_dump('ah bah ok');die;
      }
    }
  }

  // --- GETTERS ---
  public function getId(): ?int { return $this->id; }
  public function getMode(): int { return $this->mode; }
  public function getQuestions(): ?array { return $this->questions; }
  public function getUser1(): User { return $this->user1; }
  public function getUser2(): ?User { return $this->user2; }
  public function getStartAt(): \DateTime { return $this->startAt; }
  public function getWinner(): ?User { return $this->winner; }

  // --- SETTERS ---
  public function setId(int $id): Quizz
  {
    $this->id = $id;
    return $this;
  }

  public function setMode(int $mode): Quizz 
  {
    $this->mode = $mode;
    return $this;
  }

  public function setQuestions(?array $questions): Quizz 
  {
    $this->questions = $questions;
    return $this;
  } 

  public function setUser1(User $user1): Quizz 
  {
    $this->user1 = $user1;
    return $this;
  }

  public function setUser2(?User $user2): Quizz 
  {
    $this->user2 = $user2;
    return $this;
  }

  public function setStartAt(\DateTime $startAt): Quizz 
  {
    $this->startAt = $startAt;
    return $this;
  }

  
  public function setWinner(?User $winner): Quizz 
  {
    $this->winner = $winner;
    return $this;
  }  
}
