<?php

namespace App\Action\Login;

use App\Core\Controller\AbstractController;
use App\Database\LoginDB;

class Logout extends AbstractController
{
    public function __invoke()
    {
        session_destroy();
        (new LoginDB)->clearCookies();
        return $this->render('login/login.html.twig');
    }
}
