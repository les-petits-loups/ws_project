<?php

namespace App\Action\Login;

use App\Core\Controller\AbstractController;

class Login extends AbstractController
{
    public function __invoke()
    {
        return $this->render('login/login.html.twig');
    }
}
