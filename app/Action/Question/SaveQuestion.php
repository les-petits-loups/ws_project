<?php

namespace App\Action\Question;

use App\Database\QuestionDB;
use App\Database\ThemeDB;
use App\Database\PossibleAnswerDB;
use App\Entity\Question;
use App\Entity\PossibleAnswer;
use App\Entity\Theme;
use App\Core\Controller\AbstractController;
use App\Validator\Validator;

class SaveQuestion extends AbstractController
{
    // if $id !== 0 : updating already existing question
    public function __invoke(int $id = 0)
    {   
        $themeDB = new ThemeDB();

        if (!empty($_POST['theme'])) {
            $theme = $themeDB->getThemeById((int) $_POST['theme']);
            if ($theme === false) { header('Location: /error'); }
        } else {
            $theme = new Theme();
        }
        
        $questionDB = new QuestionDB();
        $question = new Question([
            "label" => $_POST['label'],
            "theme" => $theme
        ]);

        $possibleAnswerDB = new PossibleAnswerDB();

        $errorsQuestion = [];

        if ($id !== 0) { 
            $question->setId($id);
            $oldPossibleAnswers = $possibleAnswerDB->getAnswersByQuestion($question);
            if ($oldPossibleAnswers === false) { header('Location: /error'); }
        }

        $validatorQuestion = new Validator();
        if (!$validatorQuestion->validate($question)) { $errorsQuestion = $validatorQuestion->getErrors(); }

        // REMINDER : One answer must be right. Not more than one.
        // going through each answer to see if they contain errors
        $counterRightAnswers = 0;
        $answers = [];
        $errorsAnswers = [];

        for ($i = 0; $i < 4; $i++) {
            $answers[$i] = new PossibleAnswer([
                "question" => $question, 
                "label" => $_POST['answerLabel' . $i],
                "isRight" => $_POST['answerIsRight' . $i] === "right" ? true : false
            ]);
            if (!empty($oldPossibleAnswers)) { $answers[$i]->setId($oldPossibleAnswers[$i]->getId()); }
            if ($answers[$i]->getIsRight()) { $counterRightAnswers++; }
            $validator = new Validator();
            if (!$validator->validate($answers[$i])) { $errorsAnswers[$i] = $validator->getErrors(); }
        }
        
        try {
            if ($id !== 0 && empty($errorsQuestion) && $errorsAnswers === [] && $counterRightAnswers === 1) {
                
                $result = $questionDB->updateQuestion($question);
                foreach ($answers as $answer) {
                    $updateAnswerResult = $possibleAnswerDB->updatePossibleAnswer($answer);
                    if ($updateAnswerResult === false) { header('Location: /error'); }
                }
                if ($result === false) { header('Location: /error'); }
                header('Location: /questions');

            } else if ($id === 0 && empty($errorsQuestion) && $errorsAnswers === [] && $counterRightAnswers === 1) { 
                
                $result = $questionId = $questionDB->addQuestion($question);
                foreach ($answers as $answer) {
                    $answer->getQuestion()->setId($questionId);
                    $addAnswerResult = $possibleAnswerDB->addPossibleAnswer($answer);
                    if ($addAnswerResult === false) { header('Location: /error'); }
                }
                header('Location: /questions');

            } else {
                // re-rendering same template, but with errors displayed
                return $this->render(
                    'question/question.html.twig',
                    [
                        'question' => $question,
                        'selectedTheme' => $theme,
                        'errorRightAnswer' => $counterRightAnswers !== 1 ? true : false,
                        'possibleAnswers' => $answers,
                        'errorsQuestion' => $errorsQuestion,
                        'errorsAnswers' => $errorsAnswers,
                    ]
                );
            }
        } catch (\Exception $e) {
            header('Location: /error');
        }
        
    }
}
