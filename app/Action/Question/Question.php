<?php

namespace App\Action\Question;

use App\Database\QuestionDB;
use App\Database\PossibleAnswerDB;
use App\Entity\Question as QuestionEntity;
use App\Core\Controller\AbstractController;

class Question extends AbstractController
{
    public function __invoke($id = 0)
    {
        $questionDB = new QuestionDB();
        $possibleAnswerDB = new PossibleAnswerDB();

        if ($id !== 0) {
            $question = $questionDB->getQuestionById((int) $id);
            $possibleAnswers = $possibleAnswerDB->getAnswersByQuestion($question);

                if ($question === false && $possibleAnswers === false) {
                    header('Location: /error');
                }

        } else {
            $question = new QuestionEntity();
            $possibleAnswers = [];
        }
        
        return $this->render(
            'question/question.html.twig',
            [
                'question' => $question,
                'selectedTheme' => $question->getTheme(),
                'possibleAnswers' => $possibleAnswers,
            ]
        );
    }
}
