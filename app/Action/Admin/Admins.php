<?php

namespace App\Action\Admin;

use App\Core\Controller\AbstractController;
use App\Database\AdminDB;

class Admins extends AbstractController
{
	public function __invoke(int $curr_page = 1)
	{
		$adminDB = new AdminDB();
		$total_records = (int) $adminDB->getTableSize();
		$total_pages = (int) ceil($total_records / 20);
		
		if ($curr_page <= 0 || $curr_page > $total_pages) {
			$curr_page = 1;
		}
		
		$admins = $adminDB->getAll($curr_page);

		if ($admins !== false && $total_records !== false) {
			return $this->render(
				'admin/admins.html.twig',
				[
					'curr_page' => $curr_page,
					'total_pages' => $total_pages,
					'admins' => $admins,
				]
			);
		} else {
			header('Location: /error');
		}
		
	}
}
