<?php

namespace App\Action\Admin;

use App\Core\Controller\AbstractController;
use App\Database\AdminDB;

class DeleteAdmin extends AbstractController
{
    public function __invoke(int $adminId)
    {
        $adminDB = new AdminDB();
        $admin = $adminDB->getAdminById($adminId);

        if ($admin !== false && $adminDB->deleteAdminById($adminId)) {
            if ($this->getRequest()->getMethod() === 'GET') {
                header('Location: /admins');
                exit(0);
            }
            exit(0);
        } else {
            header('Location: /error');
        }
    }
}