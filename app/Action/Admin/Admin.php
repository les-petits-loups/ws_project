<?php

namespace App\Action\Admin;

use App\Database\AdminDB;
use App\Core\Controller\AbstractController;

class Admin extends AbstractController
{
    public function __invoke($id = 0)
    {
        $admin = (new AdminDB)->getAdminById((int)$id);

        if ($admin !== false) {
            return $this->render(
                'admin/admin.html.twig',
                [
                    'admin' => $admin,
                    'error' => $admin === false ? 'true' : 'false',
                ]
            );
        } else {
            header('Location: /error');
        }

    }
}
