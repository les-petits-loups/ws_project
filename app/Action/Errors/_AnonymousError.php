<?php

namespace App\Action\Errors;

use App\Core\Controller\AbstractController;

class _AnonymousError extends AbstractController
{
    public function __invoke()
    {
        return $this->render('errors/error.html.twig', [
            "errorCode" => "ERREUR",
            "message" => "Une erreur est survenue."
        ]);
    }
}
