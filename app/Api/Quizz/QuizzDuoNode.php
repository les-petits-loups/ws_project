<?php

namespace App\Api\Quizz;
use Hoa\Websocket\Node as NodeHOA;

class QuizzDuoNode extends NodeHOA
{
    protected $_id = null;

    public function setId (int $id)
    {
        $this->_id = $id;
    }

    public function getId()
    {
        return $this->_id;
    }
}