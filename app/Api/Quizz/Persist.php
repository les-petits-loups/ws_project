<?php

namespace App\Api\Quizz;

use App\Core\Controller\AbstractController;
use App\Entity\Quizz;
use App\Serializer\ObjectSerializer;
use App\Database\QuizzDB;
use App\Database\UserDB;
use App\Database\TokenDB;

class Persist extends AbstractController
{
	public function __invoke(int $id = null)
	{
    $quizzDB = new QuizzDB();
    $tokenDB = new TokenDB();
    $userDB = new UserDB();
    
    $serializer = new ObjectSerializer();

    $request = $this->getRequest();
    $isUpdate = $request->getMethod() === 'PUT';

    $headers = $request->getHeaders();
    $authorizationHeader = $headers["Authorization"][0];
    $authorizationData = $tokenDB->decode($authorizationHeader);

    $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

    if ($isUpdate && !$id) {
      $data["error"] = "Mauvaise requête.";
      http_response_code(400);
      return json_encode($data);
    }

    if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {

      $quizzData = json_decode($request->getBody()->getContents(), true);
        
      $quizzData['startAt'] = new \DateTime($quizzData['startAt']);
      $quizzData['user1'] = $userDB->getUserById($quizzData['user1']);
      $quizzData['questions'] = explode(',',$quizzData['questions']);

      if (isset($quizzData['user2'])) {
        $quizzData['user2'] = $userDB->getUserById($quizzData['user2']);
      }

      if (isset($quizzData['winner'])) {
        $quizzData['winner'] = $userDB->getUserById($quizzData['winner']);
      }

      if ($isUpdate) {

        $quizz = $quizzDB->getQuizzById($id);

        if (!$quizz) {
          $data["error"] = "Quizz introuvable.";
          http_response_code(404);
          return json_encode($data);
        }
  
        $quizz->hydrate($quizzData);
        $quizzDB->updateQuizz($quizz);

      } else {
  
        $quizz = new Quizz();
        $quizz->hydrate($quizzData);
        $quizzDB->addQuizz($quizz);
      }

      $quizzJson = json_decode($serializer->toJson($quizz), true);
      $quizzJson['startAt'] = date('Y-m-d', strtotime($quizzJson['startAt']['date']));

      $quizzJson['user1'] = json_decode($serializer->toJson($quizz->getUser1()), true);
      unset($quizzJson['user1']['password']);
      $registerAtUser1 = date('Y-m-d', strtotime($quizzJson['user1']['registerAt']['date']));
      $quizzJson['user1']['registerAt'] = $registerAtUser1;

      if ($quizz->getUser2() !== null) {
        $quizzJson['user2'] = json_decode($serializer->toJson($quizz->getUser2()), true);
        unset($quizzJson['user2']['password']);
        $registerAtUser2 = date('Y-m-d', strtotime($quizzJson['user2']['registerAt']['date']));
        $quizzJson['user2']['registerAt'] = $registerAtUser2;
      }

      if ($quizz->getWinner() !== null) {
        $quizzJson['winner'] = json_decode($serializer->toJson($quizz->getWinner()), true);
        unset($quizzJson['winner']['password']);
        $registerAtUser1 = date('Y-m-d', strtotime($quizzJson['winner']['registerAt']['date']));
        $quizzJson['winner']['registerAt'] = $registerAtUser1;
      }
      
      $data["quizz"] = $quizzJson;
      $data["error"] = "";
      http_response_code(200);

    } else {

      $data['error'] = "Token invalide.";
      http_response_code(403);
    }
    
		return json_encode($data);
	}
}
