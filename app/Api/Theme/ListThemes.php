<?php

namespace App\Api\Theme;

use App\Core\Controller\AbstractController;
use App\Database\ThemeDB;
use App\Database\TokenDB;
use App\Serializer\ObjectSerializer;

class ListThemes extends AbstractController
{
    public function __invoke(int $id = null)
    {
        $themeDB = new ThemeDB();
        $tokenDB = new TokenDB();
        $serializer = new ObjectSerializer();

        $request = $this->getRequest();
        $params = $request->getQueryParams();

        $headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
            if (isset($params['page'])) {
                $page = intval($params['page']);
                if ($page <= 0) {
                    $page = 1;
                }
            } else {
                $page = 1;
            }
    
            if ($id === null) {
    
                if(isset($params['search'])) {
                    $search = $params['search'];
                    $themes = $themeDB->getThemesWithFourOrMoreQuestionsFiltered($page, $search);
                    $totalRecords = (int) $themeDB->getThemesWithFourOrMoreQuestionsFilteredCount($search);
                } else {
                    $themes = $themeDB->getThemesWithFourOrMoreQuestions($page);
                    $totalRecords = (int) $themeDB->getThemesWithFourOrMoreQuestionsCount();
                }
    
                $totalPages = (int) ceil($totalRecords / 20);
    
                $themes = array_map(function ($theme) use ($serializer) {
                    return $serializer->toJson($theme, false);
                }, $themes);
                if ($themes) {
                    $data["pages"] = $totalPages;
                    $data["themes"] = $themes;
                    $data["error"] = "";
                    http_response_code(200);
                } else {
                    $data["error"] = "Thèmes non trouvés";
                    http_response_code(404);
                }
    
                return json_encode($data);
    
            } else {
                $theme = $themeDB->getThemeById($id);
                if ($theme != null) {
                    $data['theme'] = json_decode($serializer->toJson($theme, true));
                    $data['error'] = "";
                    http_response_code(200);
                } else {
                    $data['error'] = "Thème non trouvé";
                    http_response_code(404);
                }
                
                return json_encode($data);
            }
        } else {
            $data['error'] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
        }
        
    }
}