<?php

namespace App\Api\PossibleAnswer;

use App\Core\Controller\AbstractController;
use App\Serializer\ObjectSerializer;
use App\Database\PossibleAnswerDB;
use App\Database\TokenDB;

class ListPossibleAnswers extends AbstractController
{
	public function __invoke(int $id = null)
	{	
        $possibleAnswerDB = new PossibleAnswerDB();
        $tokenDB = new TokenDB();
        $serializer = new ObjectSerializer();

		$request = $this->getRequest();
		$params = $request->getQueryParams();

		$headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

		if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
            if (isset($params["page"])) {
                $page = intval($params["page"]);
                if ($page <= 0) {
                    $page = 1;
                }
            } else {
                $page = 1;
            }

            if ($id === null) {
                $possibleAnswers = $possibleAnswerDB->getAll($page);
                $totalRecords = (int) $possibleAnswerDB->getTableSize();
                $totalPages = (int) ceil($totalRecords / 20);

                $possibleAnswers = array_map(function ($possibleAnswer) use ($serializer) {
                    return $serializer->toJson($possibleAnswer, false);
                }, $possibleAnswers);
    
                if ($possibleAnswers) {
                    $data["pages"] = $totalPages;
                    $data["possibleAnswers"] = $possibleAnswers;
                    $data["error"] = "";
                    http_response_code(200);
                } else {
                    $data["error"] = "Réponses non trouvées.";
                    http_response_code(404);
                }
                
                return json_encode($data);

            } else {
                $possibleAnswer = $possibleAnswerDB->getAnswerById($id);

                if ($possibleAnswer) {
                    $data["possibleAnswer"] = json_decode($serializer->toJson($possibleAnswer, true));
                    $data["error"] = "";
                    http_response_code(200);
                } else {
                    $data["error"] = "Réponse non trouvée.";
                    http_response_code(404);
                }
                
                return json_encode($data);
            }
		} else {
			$data['error'] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
		}
	}
}
