<?php
namespace App\Api\PossibleAnswer;

use App\Core\Controller\AbstractController;
use App\Serializer\ObjectSerializer;
use App\Database\PossibleAnswerDB;
use App\Database\QuestionDB;
use App\Database\TokenDB;

class ListQuestionPossibleAnswers extends AbstractController
{
	public function __invoke(int $id)
	{
        $questionDB = new QuestionDB();
        $possibleAnswerDB = new PossibleAnswerDB();
        $tokenDB = new TokenDB();
        $serializer = new ObjectSerializer();

        $request = $this->getRequest();
        $headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
            $question = $questionDB->getQuestionById($id);

            if ($question) {
                $answers = $possibleAnswerDB->getAnswersByQuestion($question);
                
                
                if ($answers) {
                    $questionPossibleAnswers = [];

                    foreach($answers as $answer) {
                        $questionPossibleAnswers[] = json_decode($serializer->toJson($answer), true);
                    }
                            
                    foreach ($questionPossibleAnswers as &$answer) {
                        $answer["question"] = json_decode($serializer->toJson($question), true);
                        // remove isRight so users can't cheat by looking at what's going on in the Network
                        unset($answer['isRight']);
                    }
    
                    $data["possibleAnswers"] = $questionPossibleAnswers;
                    $data["error"] = "";
                    http_response_code(200);
    
                } else {
                    $data["error"] = "Réponses non trouvées.";
                    http_response_code(404);
                }

            } else {
                $data["error"] = "Réponses non trouvées.";
                http_response_code(404);
            }
        
        } else {
            $data["error"] = "Token invalide.";
            http_response_code(403);
        }
        
       return json_encode($data);
	}
}