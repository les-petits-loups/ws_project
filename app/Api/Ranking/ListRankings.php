<?php

namespace App\Api\Ranking;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;
use App\Database\TokenDB;
use App\Serializer\ObjectSerializer;

class ListRankings extends AbstractController
{
    public function __invoke(int $id = null)
    {
        $userDB = new UserDB();
        $tokenDB = new TokenDB();

        $request = $this->getRequest();
        $params = $request->getQueryParams();
        $serverParams = $request->getServerParams();

        $serializer = new ObjectSerializer();

        $headers = $request->getHeaders();

        $contentType = explode(';',$serverParams['CONTENT_TYPE'])[0];
        
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
            if (isset($params['page'])) {
                $page = intval($params['page']);
                if ($page <= 0) {
                    $page = 1;
                }
            } else {
                $page = 1;
            }
    
            if ($id === null) {
                
                $rankings = $userDB->getAllRankings($page);
                $totalRecords = (int) $userDB->getTableSize();
                $totalPages = (int) ceil($totalRecords / 20);
                $numberOfUsers = $userDB->getTableSize();
                
                if ($rankings) {
                    $data["pages"] = $totalPages;
                    $data['rankings'] = $rankings;
                    $data['numberOfUsers'] = (int) $numberOfUsers;
                    $data['error'] = "";
                    http_response_code(200);
                } else {
                    $data['error'] = "Classements non trouvés";
                    http_response_code(404);
                }
                
                $this->addHeader('Content-Type', 'application/json; charset=UTF-8');
                return json_encode($data);
    
            } else {
                if ($userDB->getUserById($id)) {
                    $ranking = $userDB->getRanking($id);
                    $numberOfUsers = $userDB->getTableSize();
                    if ($ranking) {
                        $data['ranking'] = $ranking;
                        $data['numberOfUsers'] = (int) $numberOfUsers;
                        $data['error'] = "";
                        http_response_code(200);
                    } else {
                        $data['error'] = "Classement non trouvé";
                        http_response_code(404);
                    }
                } else {
                    $data['error'] = "Classement non trouvé";
                    http_response_code(404);
                }
                
                if ($contentType === "application/xml") {
                    $xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><data></data>');
                    $serializer->toXml($data, $xml_data);
                    $this->addHeader('Content-Type', 'application/xml; charset=UTF-8');
                    return $xml_data->asXML();
                    
                } else { // default is JSON
                    $this->addHeader('Content-Type', 'application/json; charset=UTF-8');
                    return json_encode($data);
                }               
            }
        } else {
            $data['error'] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
        }
        
    }
}