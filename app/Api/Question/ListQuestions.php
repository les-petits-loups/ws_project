<?php

namespace App\Api\Question;

use App\Core\Controller\AbstractController;
use App\Serializer\ObjectSerializer;
use App\Database\QuestionDB;
use App\Database\TokenDB;

class ListQuestions extends AbstractController
{
	public function __invoke(int $id = null)
	{	
		$questionDB = new QuestionDB();
		$tokenDB = new TokenDB();

		$serializer = new ObjectSerializer();

		$request = $this->getRequest();
		$params = $request->getQueryParams();

		$headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

		if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
			// Check if we want 4 random questions by Theme ;)
			if (isset($params["theme"]) && isset($params["random"]) && $params["random"] === "1") {

				$randomQuestions = $questionDB->getFourRandomQuestionsByTheme($params["theme"]);
		
				if ($randomQuestions) {
					$randomQuestionsJson = [];
					$randomQuestionsJson[] = json_decode($serializer->toJson($randomQuestions[0], true));
					$randomQuestionsJson[] = json_decode($serializer->toJson($randomQuestions[1], true));
					$randomQuestionsJson[] = json_decode($serializer->toJson($randomQuestions[2], true));
					$randomQuestionsJson[] = json_decode($serializer->toJson($randomQuestions[3], true));
					$data["questions"] = $randomQuestionsJson; 
					$data["error"] = "";
					http_response_code(200);
				} else {
					$data["error"] = "Le thème ou les questions n'existent pas.";
					http_response_code(404);
				}

				return json_encode($data);
			} else {

				if (isset($params["page"])) {
					$page = intval($params["page"]);
					if ($page <= 0) {
						$page = 1;
					}
				} else {
					$page = 1;
				}

				if ($id === null) {
					$questions = $questionDB->getAll($page);
					$totalRecords = (int) $questionDB->getTableSize();
					$totalPages = (int) ceil($totalRecords / 20);

					$questions = array_map(function ($question) use ($serializer) {
						return $serializer->toJson($question, false);
					}, $questions);
		
					if ($questions) {
					  $data["pages"]=$totalPages;
						$data["questions"] = $questions;
						$data["error"] = "";
						http_response_code(200);
					} else {
						$data["error"] = "Questions non trouvées.";
						http_response_code(404);
					}
					
					return json_encode($data);

				} else {
					$question = $questionDB->getQuestionById($id);

					if ($question) {
						$data["question"] = json_decode($serializer->toJson($question, true));
						$data["error"] = "";
						http_response_code(200);
					} else {
						$data["error"] = "Question non trouvée.";
						http_response_code(404);
					}
					
					return json_encode($data);
				}
			}
		} else {
			$data['error'] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
		}
	}
}
