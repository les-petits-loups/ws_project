<?php

namespace App\Api\User;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;
use App\Database\TokenDB;
use App\Serializer\ObjectSerializer;

class ListUsers extends AbstractController
{
    public function __invoke(int $id = null)
    {
        $userDB = new UserDB();
        $tokenDB = new TokenDB();
        
        $serializer = new ObjectSerializer();

        $request = $this->getRequest();
        $params = $request->getQueryParams();
        $serverParams = $request->getServerParams();

        $contentType = explode(';',$serverParams['CONTENT_TYPE'])[0];

        if (!isset($params['register'])) {
            $headers = $request->getHeaders();
            $authorizationHeader = $headers["Authorization"][0];
            $authorizationData = $tokenDB->decode($authorizationHeader);
        }
       
        if ((isset($params['register']) && $params['register'] === "1") 
        || $authorizationData && $authorizationData["exp"] && $authorizationData["iat"] ) {

            if (isset($params['page'])) {
                $page = intval($params['page']);
                if ($page <= 0) {
                    $page = 1;
                }
            } else {
                $page = 1;
            }
    
            if ($id === null) {

                if (isset($params['search'])) {
                    $search = (string) $params['search'];
                    $users = $userDB->getUsersFilteredByEmail($page, $search);
                    $totalRecords = (int) $userDB->getUsersFilteredByEmailTableSize($search);
                } else {
                    $users = $userDB->getAll($page);
                    $totalRecords = (int) $userDB->getTableSize();
                }
                
                $totalPages = (int) ceil($totalRecords / 20);

                $users = array_map(function ($user) use ($serializer) {
                    return $serializer->toJson($user, false);
                }, $users);

                if ($users) {
                    $data["pages"] = $totalPages;
                    $data["users"] = $users;

                    foreach($data["users"] as &$user) {
                        unset($user["password"]);
                        $user['registerAt'] = $user['registerAt']->format('Y-m-d');
                    }
                    
                    $data["error"] = "";
                    http_response_code(200);
                } else {
                    $data["error"] = "Utilisateurs non trouvés.";
                    http_response_code(404);
                }
                
                $this->addHeader('Content-Type', 'application/json; charset=UTF-8');
                return json_encode($data);
    
            } else {
                $user = $userDB->getUserById($id);
                if ($user != null) {
                    $data['user'] = json_decode($serializer->toJson($user), true);
                    unset($data['user']['password']);
                    $data['user']['registerAt'] = date('Y-m-d', strtotime($data['user']['registerAt']['date']));
                    $data['error'] = "";
                    http_response_code(200);
                } else {
                    $data['error'] = "Utilisateur non trouvé.";
                    http_response_code(404);
                }

                if ($contentType === "application/xml") {
                    $xml_data = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><data></data>');
                    $serializer->toXml($data, $xml_data);
                    $this->addHeader('Content-Type', 'application/xml; charset=UTF-8');
                    return $xml_data->asXML();
                } else { // default is JSON
                    $this->addHeader('Content-Type', 'application/json; charset=UTF-8');
                    return json_encode($data);
                }       
            }
        } else {
            $data['error'] = "Token invalide.";
            http_response_code(403);
            $this->addHeader('Content-Type', 'application/json; charset=UTF-8');
            return json_encode($data);
        }
    }
}
