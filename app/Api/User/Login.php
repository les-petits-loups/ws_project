<?php

namespace App\Api\User;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;
use App\Database\TokenDB;
use App\Core\Config\Config;

class Login extends AbstractController
{
	public function __invoke()
	{
		$userDB = new UserDB();
		$tokenDB = new TokenDB();

		$request = $this->getRequest();

		$this->addHeader('Content-Type', 'application/json; charset=UTF-8');

		$requestBody = json_decode($request->getBody()->getContents(), true);
		$user = $userDB->getUserByEmail($requestBody['email']);

		if (!$user) {
			$data["error"] = "Identifiants non valides.";
			http_response_code(404);
			return json_encode($data);
		}

		if (!password_verify($requestBody['password'], $user->getPassword())) {
			$data["error"] = "Identifiants non valides.";
			http_response_code(404);
			return json_encode($data);
		}

		$virtualHost = Config::config('virtual_host');

		$data['token'] = $tokenDB->encode([
            "iss" => "http:// " . $virtualHost,
            "aud" => "http:// " . $virtualHost,
            "iat" => time(),
            "exp" => time() + 86400,
			"email" => $requestBody['email'],
			"id" => $user->getId(),
			"connected" => true

		]);
		$data['id'] = $user->getId();
		$data['error'] = "";
		
        return json_encode($data);
	}
}
