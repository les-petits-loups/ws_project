<?php

namespace App\Api\User;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;
use App\Database\TokenDB;

class Token extends AbstractController
{
	public function __invoke()
	{
		$userDB = new UserDB();
		$tokenDB = new TokenDB();

		$request = $this->getRequest();
        $requestBody = json_decode($request->getBody()->getContents(), true);

        $headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        $tokenData = $tokenDB->decode($requestBody['token']);
        
        if ($tokenData && $authorizationData["exp"] && $authorizationData["iat"]) {
            $user = $userDB->getUserByEmail($tokenData['email']);

            if (!$user 
                || $user->getId() !== $tokenData['id'] 
                || strtolower($user->getEmail()) !== strtolower($tokenData['email'])
                || $tokenData['connected'] === false) {
                
                $data["error"] = "Token invalide.";
                http_response_code(403);
                return json_encode($data);
            }
            
            $data['id'] = $user->getId();
            $data['error'] = "";
            return json_encode($data);
        } else {
            $data["error"] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
        }
        
	}
}
