<?php

namespace App\Api\User;

use App\Core\Controller\AbstractController;
use App\Database\UserDB;
use App\Database\TokenDB;

class Delete extends AbstractController
{
    public function __invoke(int $id)
    {
        $userDB = new UserDB();
        $tokenDB = new TokenDB();
        $user = $userDB->getUserById($id);

        $request = $this->getRequest();
        $headers = $request->getHeaders();
        $authorizationHeader = $headers["Authorization"][0];
        $authorizationData = $tokenDB->decode($authorizationHeader);

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {
            if (!$user) {
                $data['error'] = "Utilisateur non trouvé.";
                http_response_code(404);
                return json_encode($data);
            }
    
            $result = $userDB->deleteUser($user);
    
            if ($result) {
                $data['error'] = "";
                http_response_code(200);
                return json_encode($data);
            }
    
            $data['error'] = "Erreur interne du serveur.";
            http_response_code(500);
            return json_encode($data);
        } else {
            $data['error'] = "Token invalide.";
            http_response_code(403);
            return json_encode($data);
        }
        
    }
}
