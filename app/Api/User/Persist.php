<?php

namespace App\Api\User;

use App\Core\Controller\AbstractController;
use App\Entity\User;
use App\Serializer\ObjectSerializer;
use App\Database\UserDB;
use App\Database\TokenDB;
use App\Validator\Validator;
use DateTime;

class Persist extends AbstractController
{
    public function __invoke(int $id = null)
    {
        $userDB = new UserDB();
        $tokenDB = new TokenDB();
        $serializer = new ObjectSerializer();

        $request = $this->getRequest();
        $isUpdate = $request->getMethod() === 'PUT';

        $this->addHeader('Content-Type', 'application/json; charset=UTF-8');

        if ($isUpdate && !$id) {
            $data["error"] = "Mauvaise requête.";
            http_response_code(400);
            return json_encode($data);
        }

        if ($isUpdate) {

            $headers = $request->getHeaders();
            $authorizationHeader = $headers["Authorization"][0];
            $authorizationData = $tokenDB->decode($authorizationHeader);

            if ($authorizationData && $authorizationData["exp"] && $authorizationData["iat"]) {

                $user = $userDB->getUserById($id);

                if (!$user) {
                    $data["error"] = "Utilisateur non trouvé.";
                    http_response_code(404);
                    return json_encode($data);
                }

                $requestData = json_decode($request->getBody()->getContents(), true);
                $requestData['registerAt'] = new DateTime($requestData['registerAt']);
                $requestData['password'] = password_hash($requestData['password'], PASSWORD_ARGON2I);
                $user->hydrate($requestData);
                $userDB->updateUser($user);

                $data['user'] = json_decode($serializer->toJson($user), true);
                unset($data['user']['password']);
                $data['user']['registerAt'] = date('Y-m-d', strtotime($data['user']['registerAt']['date']));
                $data['error'] = "";
            } else {
                $data['error'] = "Token invalide.";
                http_response_code(403);
                return json_encode($data);
            }
            
        } else {

            $requestData = json_decode($request->getBody()->getContents(), true);
            $requestData['registerAt'] = new DateTime();
            $requestData['password'] = password_hash($requestData['password'], PASSWORD_ARGON2I);
            $requestData['points'] = 0;
            $user = new User();
            $user->hydrate($requestData);
            $validator = new Validator();
            $isValid = $validator->validate($user);

            if ($isValid) {
                $userDB->addUser($user);

                $data['user'] = json_decode($serializer->toJson($user), true);
                unset($data['user']['password']);
                $data['user']['registerAt'] = date('Y-m-d', strtotime($data['user']['registerAt']['date']));
                $data['error'] = "";

                $message            = new \Hoa\Mail\Message();
                $message['From']    = 'noreply-projectJS <kamilmackow98@gmail.com>'; // The value between "<" & ">" is overridden by GMAIL anyway but it's need to be here in order to display the "noreply-projectJS"
                $message['To']      = $requestData['firstName'] . ' ' . $requestData['lastName'] . '  <' . $requestData['email'] . '>';
                $message['Subject'] = 'Bienvenue !';

                $message->addContent(
                    new \Hoa\Mail\Content\Text('Bienvenue sur l\'application de Quizz que nous avons développé !')
                );

                $message->send();
            } else {
                $data['error'] = "Informations envoyées invalides.";
                http_response_code(403);
                return json_encode($data);
            }
        }
        
        http_response_code(200);
        return json_encode($data);
    }
}
