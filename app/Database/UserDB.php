<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\User;
use PDOException;

class UserDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAll(int $page)
    {
        try {

            $pagination = ($page - 1) * 20;
            $query = 'SELECT * FROM `User` LIMIT 20 OFFSET :pagination';

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['registerAt'] = new \DateTime($value['registerAt']);
                    $result[] = new User($value);
                }

                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getUsersFilteredByEmail(int $page, string $search)
    {
        $pagination = ($page - 1) * 20;
        $search .= "%";
        $query = 'SELECT * FROM `User` WHERE email LIKE :search LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('search', $search, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = [];

                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $value['registerAt'] = new \DateTime($value['registerAt']);
                    $result[] = new User($value);
                }

                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getUserById(int $id): ?User
    {
        $query = 'SELECT * FROM `User` WHERE id = ?';

        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['registerAt'] = new \DateTime($result['registerAt']);
                    return new User($result);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getUserByEmail(string $email): ?User
    {
        $query = 'SELECT * FROM `User` WHERE email = ?';

        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$email]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['registerAt'] = new \DateTime($result['registerAt']);
                    return new User($result);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `User`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getUsersFilteredByEmailTableSize(string $search) 
    {
        $searchTerm = $search . "%";
        $query = 'SELECT COUNT(*) FROM `User` WHERE email LIKE :search';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('search', $searchTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addUser(User $user)
    {
        try {
            $args = [
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmail(),
                $user->getRegisterAt()->format('Y-m-d H:i:s'),
                $user->getPassword(),
                $user->getPoints()
            ];

            $query = "INSERT INTO User(firstName, lastName, email, registerAt, password, points) VALUES (?,?,?,?,?,?)";
            $this->pdoStatement = $this->pdo->prepare($query);
            $results = $this->pdoStatement->execute($args);

            if ($results !== false) {
                $user->setId($this->pdo->lastInsertId());
                return $user;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateUser(User $user)
    {
        try {
            $args = [
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmail(),
                $user->getRegisterAt()->format('Y-m-d H:i:s'),
                $user->getPassword(),
                $user->getPoints(),
                $user->getId()
            ];

            $query = "UPDATE User SET firstName = ?, lastName = ?, email = ?, registerAt = ?, password = ?, points = ? WHERE id = ?";

            $this->pdoStatement = $this->pdo->prepare($query);
            $results = $this->pdoStatement->execute($args);

            if (true === $results) {
                return $user;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }

    }

    public function deleteUser(User $user): bool
    {
        try {
            $query = 'DELETE FROM User WHERE id = ?';
            $this->pdoStatement = $this->pdo->prepare($query);

            return $this->pdoStatement->execute([$user->getId()]);
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }

    }

    public function getAllRankings(int $page)
    {
        try {

            $pagination = ($page - 1) * 20;
            $query = 'SELECT id,points FROM User ORDER BY points DESC,id LIMIT 20 OFFSET :pagination';

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];
            if ($valid) {
                $ranking = $pagination;
                $ranking++;
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $oneRanking["idUser"] = intval($value["id"]);
                    $oneRanking["ranking"] = $ranking++;
                    $result[] = $oneRanking;
                }
                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return null;
        }
    }

    public function getRanking(int $id)
    {
        $page = 1;
        $idExists = true;
        while ($idExists) {
            $rankings = $this->getAllRankings($page);
            if (count($rankings) === 0) {
                $idExists = false;
            }
            foreach ($rankings as $value) {
                if ($value['idUser'] === $id) {
                    return $value['ranking'];
                }
            }
            $page++;
        }
        return null;
    }
}
