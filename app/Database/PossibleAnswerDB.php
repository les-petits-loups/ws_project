<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\Question;
use App\Entity\PossibleAnswer;
use App\Database\QuestionDB;
use PDOException;

class PossibleAnswerDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAnswersByQuestion(Question $question)
    {
        $id = $question->getId();
        $query = 'SELECT * FROM `PossibleAnswer` WHERE question=:id';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $value['question'] = $question;
                    $result[] = new PossibleAnswer($value);
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getAnswerById(int $id)
    {
        $questionDB = new QuestionDB();
        $query = 'SELECT * FROM `PossibleAnswer` WHERE id=:id';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
                $result['question'] = $questionDB->getQuestionById($result['question']);
                return new PossibleAnswer($result);
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addPossibleAnswer(PossibleAnswer $answer): bool
    {
        $questionId = $answer->getQuestion()->getId();
        $label = $answer->getLabel();
        $isRight = $answer->getIsRight();

        $query = 'INSERT INTO PossibleAnswer(question, label, isRight)
        VALUES(:question, :label, :isRight)';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('question', $questionId, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('isRight', $isRight, \PDO::PARAM_BOOL);

            $valid = $this->pdoStatement->execute();
            
            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updatePossibleAnswer(PossibleAnswer $answer): bool
    {
        $questionId = $answer->getQuestion()->getId();
        $label = $answer->getLabel();
        $isRight = $answer->getIsRight();
        $id = $answer->getId();

        $query = 'UPDATE PossibleAnswer 
                SET label=:label,
                isRight=:isRight,
                question=:question
                WHERE id=:id';
       
        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('question', $questionId, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('label', $label, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('isRight', $isRight, \PDO::PARAM_BOOL);
            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);


            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `PossibleAnswer`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getAll(int $page)
    {
        $questionDB = new QuestionDB();
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `PossibleAnswer` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['question'] = $questionDB->getQuestionById($value['question']);
                    $result[] = new \App\Entity\Question($value);
                }

                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }
}
