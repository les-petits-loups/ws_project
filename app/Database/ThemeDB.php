<?php

namespace App\Database;

use App\Entity\Theme;
use App\Core\Controller\AbstractController;
use PDOException;

class ThemeDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function isThemeUnique(string $name, int $id): bool
    {
        $query = 'SELECT * FROM `Theme` WHERE name = ? AND id <> ?';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$name, $id]);

            if ($valid) {
                if ($this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    return false;
                }
            }
            return true;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addTheme(Theme $theme): bool
    {
        $name = $theme->getName();

        $query = 'INSERT INTO Theme(name)
        VALUES(:name)';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('name', $name, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateTheme(Theme $theme): bool
    {
        $name = $theme->getName();
        $id = $theme->getId();

        $query = 'UPDATE Theme 
                SET name=:name
                WHERE id=:id';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('name', $name, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getThemeById(int $id)
    {
        $query = 'SELECT * FROM `Theme` WHERE id = ?';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    $result['id'] = intval($result['id']);
                    $result = new Theme($result);
                    return $result;
                } else {
                    return null;
                }
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getPaginatedThemes(string $term, int $page)
    {
        $pagination = ($page - 1) * 20;
        $searchTerm = $term . "%";
        $themesCount = $this->getThemesCount($searchTerm);
        $query = 'SELECT * FROM `Theme` WHERE name LIKE :term LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('term', $searchTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = [];
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $result[] = new Theme($value);
                }

                return ["results" => $result, "count" => $themesCount];
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getThemesCount(string $term)
    {
        $query = 'SELECT COUNT(*) FROM `Theme` WHERE name LIKE :term';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('term', $term, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
                return $result['COUNT(*)'];
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function deleteThemeById(int $id): bool
    {
        $query = 'DELETE FROM Theme WHERE id=:id';

        try {

            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getAll(int $page)
    {
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Theme` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = [];

                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $value['id'] = intval($value['id']);
                    $result[] = new Theme($value);
                }

                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `Theme`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }

        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }

    }

    public function getThemesWithFourOrMoreQuestions(int $page)
    {
        $pagination = ($page - 1) * 20;
        $query = 'SELECT Question.theme, Theme.name FROM Question INNER JOIN Theme on Question.theme = Theme.id GROUP BY Question.theme HAVING COUNT(theme)>3 limit 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $response = [];
                    $response['id'] = intval($value['theme']);
                    $response['name'] = $value['name'];
                    $result[] = new Theme($response);
                }
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getThemesWithFourOrMoreQuestionsCount() 
    {
        $query = 'SELECT COUNT(*) FROM (SELECT Question.theme, Theme.name FROM Question INNER JOIN Theme on Question.theme = Theme.id GROUP BY Question.theme HAVING COUNT(theme)>3) temp';
        
        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getThemesWithFourOrMoreQuestionsFiltered(int $page, String $filter) 
    {
        $pagination = ($page - 1) * 20;
        $filter .= '%';

        $query = "SELECT Question.theme, Theme.name FROM Question INNER JOIN Theme on Question.theme = Theme.id WHERE Theme.name LIKE :filter GROUP BY Question.theme HAVING COUNT(theme)>3 limit 20 OFFSET :pagination";

        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            
            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('filter', $filter, \PDO::PARAM_STR);
            
            $valid = $this->pdoStatement->execute();
            $result = [];
            
            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $response = [];
                    $response['id'] = intval($value['theme']);
                    $response['name'] = $value['name'];
                    $result[] = new Theme($response);
                }
                return $result;
            } else {
                return $valid;
            }

            
        } catch(PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getThemesWithFourOrMoreQuestionsFilteredCount(String $filter)
    {
        $filter .= '%';
        $query = "SELECT COUNT(*) FROM (SELECT Question.theme, Theme.name FROM Question INNER JOIN Theme on Question.theme = Theme.id WHERE Theme.name LIKE :filter GROUP BY Question.theme HAVING COUNT(theme)>3) temp";

        try {
            $this->pdoStatement = $this->pdo->prepare($query);
            $this->pdoStatement->bindParam('filter', $filter, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch(PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

}
