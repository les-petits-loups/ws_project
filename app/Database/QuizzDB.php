<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use App\Entity\Quizz;
use App\Database\UserDB;
use PDOException;

class QuizzDB extends AbstractController
{
    private $pdo;
    private $pdoStatement;

    public function __construct()
    {
        $this->pdo = $this->getConnection();
    }

    public function getAll(int $page)
    {
        $userDB = new UserDB();
        $pagination = ($page - 1) * 20;
        $query = 'SELECT * FROM `Quizz` LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];
            $values = []; 

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $values[] = $value;   
                }
    
                foreach($values as $value) {
                    $quizz = new Quizz([
                            'id' => $value['id'],
                            'mode' => $value['mode'],
                            'user1' => $userDB->getUserById($value['user1']),
                            'user2' => $value['user2'] === null ? null : $userDB->getUserById($value['user2']),
                            'winner' => $value['winner'] === null ? null : $userDB->getUserById($value['winner']),
                            'startAt' => new \DateTime($value['startAt']),
                            'questions' => $this->getQuizzQuestions($value['id'])
                    ]);

                    $result[] = $quizz;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getQuizzQuestions(int $idQuizz)
    {
        $query = 'SELECT * FROM `Quizz_Question` WHERE id_quizz = :idQuizz';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('idQuizz', $idQuizz, \PDO::PARAM_INT);

            $valid = $this->pdoStatement->execute();
            $result = [];

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $result[] = $value;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getQuizzsFiltered(int $page, String $filter)
    {
        $userDB = new UserDB();
        $pagination = ($page - 1) * 20;
        $filterTerm = $filter . "%";
        $query = 'SELECT Quizz.id, Quizz.mode, Quizz.user1, Quizz.user2, Quizz.winner, Quizz.startAt FROM Quizz, User  
                WHERE ( user1 = User.id OR user2 = User.id )
                AND ( ( firstName LIKE :filterTerm ) OR ( lastName LIKE :filterTerm )  OR ( email LIKE :filterTerm ) )
                LIMIT 20 OFFSET :pagination';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('pagination', $pagination, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('filterTerm', $filterTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();
            $result = [];
            $values = []; 

            if ($valid) {
                while ($value = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $values[] = $value;   
                }
    
                foreach($values as $value) {
                    $quizz = new Quizz([
                            'id' => $value['id'],
                            'mode' => $value['mode'],
                            'user1' => $userDB->getUserById($value['user1']),
                            'user2' => $value['user2'] === null ? null : $userDB->getUserById($value['user2']),
                            'winner' => $value['winner'] === null ? null : $userDB->getUserById($value['winner']),
                            'startAt' => new \DateTime($value['startAt']),
                            'questions' => $this->getQuizzQuestions($value['id'])
                    ]);
                    $result[] = $quizz;
                }
    
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSizeFiltered(String $filter) {
        $filterTerm = $filter . "%";
        $query = 'SELECT COUNT(*) FROM Quizz, User  
                WHERE ( user1 = User.id OR user2 = User.id )
                AND ( ( firstName LIKE :filterTerm ) OR ( lastName LIKE :filterTerm )  OR ( email LIKE :filterTerm ) )';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('filterTerm', $filterTerm, \PDO::PARAM_STR);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result; 
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getTableSize()
    {
        $query = 'SELECT COUNT(*) FROM `Quizz`';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute();

            if ($valid) {
                $result = $this->pdoStatement->fetchColumn();
                return $result;
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function getQuizzById(int $id)
    {
        $userDB = new UserDB();
        $query = 'SELECT * FROM `Quizz` WHERE id = ?';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $valid = $this->pdoStatement->execute([$id]);

            if ($valid) {
                $result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

                if ($result !== false) {
                    return new Quizz([
                        'id' => $result['id'],
                        'mode' => $result['mode'],
                        'user1' => $userDB->getUserById($result['user1']),
                        'user2' => $result['user2'] === null ? null : $userDB->getUserById($result['user2']),
                        'winner' => $result['winner'] === null ? null : $userDB->getUserById($result['winner']),
                        'startAt' => new \DateTime($result['startAt']),
                        'questions' => $this->getQuizzQuestions($result['id'])
                    ]);
                } else {
                    return null;
                }
            } else {
                return $valid;
            }
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return null;
        }
    }
    
    public function addQuizzQuestions(int $idQuizz, array $questions) {
        $query = 'INSERT INTO Quizz_Question(id_quizz, id_question)
        VALUES(:id_quizz, :id_question)';

        try {
                $this->pdoStatement = $this->pdo->prepare($query);

                foreach($questions as $question) {

                    $this->pdoStatement->bindParam('id_quizz', $idQuizz, \PDO::PARAM_INT);
                    $this->pdoStatement->bindParam('id_question', $question, \PDO::PARAM_INT);
                    $valid = $this->pdoStatement->execute();
                }
                
            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function addQuizz(Quizz $quizz)
    {
        $mode = $quizz->getMode();
        $user1 = $quizz->getUser1()->getId();
        $startAt = $quizz->getStartAt()->format('Y-m-d H:i:s');
        $user2 = $mode === 2 ? $quizz->getUser2()->getId() : null;
        $winner = $quizz->getWinner() !== null ? $quizz->getWinner()->getId() : null;

        $query = 'INSERT INTO Quizz(mode, user1, user2, startAt, winner)
        VALUES(:mode, :user1, :user2, :startAt, :winner)';

        try {
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('mode', $mode, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('user1', $user1, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('user2', $user2, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('startAt', $startAt, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('winner', $winner, \PDO::PARAM_INT);
            
            $valid = $this->pdoStatement->execute();
            $questions = $quizz->getQuestions();
            $quizz->setId($this->pdo->lastInsertId());

            if ($valid) {
                $this->addQuizzQuestions($this->pdo->lastInsertId(), $questions);
                return $quizz;
            } else {
                return $valid;
            }
            
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateQuizz(Quizz $quizz): bool
    {
        $id = $quizz->getId();
        $mode = $quizz->getMode();
        $user1 = $quizz->getUser1()->getId();
        $startAt = $quizz->getStartAt()->format('Y-m-d H:i:s');
        $user2 = $mode === 2 ? $quizz->getUser2()->getId() : null;
        $winner = $mode === 2 ? $quizz->getWinner()->getId() : null;

        $query = 'UPDATE Quizz
        SET mode=:mode, 
        user1=:user1, 
        user2=:user2, 
        startAt=:startAt, 
        winner=:winner
        WHERE id=:id';

        try {
            
            $this->pdoStatement = $this->pdo->prepare($query);

            $this->pdoStatement->bindParam('id', $id, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('mode', $mode, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('user1', $user1, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('user2', $user2, \PDO::PARAM_INT);
            $this->pdoStatement->bindParam('startAt', $startAt, \PDO::PARAM_STR);
            $this->pdoStatement->bindParam('winner', $winner, \PDO::PARAM_INT);
            
            $valid = $this->pdoStatement->execute();

            $newQuestions = $quizz->getQuestions();
            $this->updateQuizzQuestions($id, $newQuestions);
            
            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }

    public function updateQuizzQuestions(int $idQuizz, array $newQuestions) {
        $quizz = $this->getQuizzById($idQuizz);
        $oldQuestions = $quizz->getQuestions();

        $query = 'UPDATE Quizz_Question
        SET id_quizz=:id_quizz, 
        id_question=:id_question
        WHERE id_quizz=:id_quizz
        AND id_question=:id_old_question';

        try {
                $this->pdoStatement = $this->pdo->prepare($query);
                $index = 0;
                foreach($newQuestions as $idNewQuestion) {
                    $idOldQuestion = $oldQuestions[$index]["id_question"];
                    $this->pdoStatement->bindParam('id_quizz', $idQuizz, \PDO::PARAM_INT);
                    $this->pdoStatement->bindParam('id_question', $idNewQuestion, \PDO::PARAM_INT);
                    $this->pdoStatement->bindParam('id_old_question', $idOldQuestion, \PDO::PARAM_INT);

                    $valid = $this->pdoStatement->execute();
                    $index++;
                }

            return $valid;
        } catch (PDOException $e) {
            handleSqlErrors($query, $e->getMessage());
            return false;
        }
    }
}
