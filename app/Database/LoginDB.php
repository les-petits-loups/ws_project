<?php

namespace App\Database;

use App\Core\Controller\AbstractController;
use PDOException;

class LoginDB extends AbstractController
{

	private \PDO $pdo;
	private \PDOStatement $pdoStatement;
	private int $time = 30 * 24 * 60 * 60; // Time for one month
	private string $adminName;
	private int $adminId;

	public function __construct()
	{
		$this->pdo = $this->getConnection();
	}

	public function isLoginInfoCorrect(string $email, string $password): bool
	{

		$query = 'SELECT * FROM `Admin` WHERE email = ?';

		try {
			$this->pdoStatement = $this->pdo->prepare($query);
			$valid = $this->pdoStatement->execute([$email]);

			if ($valid) {
				$admin = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
				if (password_verify($password, $admin['password'])) {
					$this->adminName = $admin['firstName'];
					$this->adminId = $admin['id'];
					$this->loginAdmin($admin);
					return true;
				}
			}
			return false;
		} catch (PDOException $e) {
			handleSqlErrors($query, $e->getMessage());
			return false;
		}
	}

	public function loginAdmin($admin)
	{
		$_SESSION['logged'] = true;
		$_SESSION['id'] = $admin['id'];
		$_SESSION['firstName'] = $admin['firstName'];
	}

	public function rememberMe()
	{
		$currentTime = time();
		$cookieExpiration = $currentTime + $this->time;

		// Set cookies
		setcookie("admin_id", $this->adminId, $cookieExpiration);
		setcookie("admin_firstName", $this->adminName, $cookieExpiration);

		$token = $this->generateToken(16);
		setcookie("token", $token, $cookieExpiration);

		$selector = $this->generateToken(32);
		setcookie("selector", $selector, $cookieExpiration);

		$hashedToken = password_hash($token, PASSWORD_DEFAULT);
		$hashedSelector = password_hash($selector, PASSWORD_DEFAULT);

		$expiresDate = date("Y-m-d H:i:s", $cookieExpiration);
		$adminToken = $this->getTokenByAdminID($this->adminId);

		// Check if token already exists for adminId and delete then assign a new one
		if (!empty($adminToken['id'])) {
			$delete = $this->deleteToken($adminToken['id']);

			if ($delete === false) {
				header("Location: /error");
			}
		}

		$insert = $this->insertToken($hashedToken, $hashedSelector, $this->adminId, $expiresDate);

		if ($insert === false) {
			header("Location: /error");
		}
	}

	public function clearCookies()
	{
		unset($_COOKIE["admin_id"]);
		unset($_COOKIE["admin_firstName"]);
		unset($_COOKIE["token"]);
		unset($_COOKIE["selector"]);

		// To be sure that the value of the cookie is empty
		setcookie("admin_id", "", time() - 3600);
		setcookie("admin_firstName", "", time() - 3600);
		setcookie("token", "", time() - 3600);
		setcookie("selector", "", time() - 3600);
	}

	private function generateToken(int $length = 16)
	{
		return bin2hex(random_bytes($length));
	}

	public function getTokenByAdminID(int $adminId)
	{
		$query = "SELECT * FROM `Token_Admin` WHERE admin = ?";

		try {
			$this->pdoStatement = $this->pdo->prepare($query);
			$this->pdoStatement->execute([$adminId]);
			$result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);

			return $result;
		} catch (PDOException $e) {
			handleSqlErrors($query, $e->getMessage());
			return false;
		}
	}

	public function deleteToken(int $id): bool
	{
		$query = "DELETE FROM `Token_Admin` WHERE id = ?";

		try {
			$this->pdoStatement = $this->pdo->prepare($query);
			$valid = $this->pdoStatement->execute([$id]);

			return $valid;
		} catch (PDOException $e) {
			handleSqlErrors($query, $e->getMessage());
			return false;
		}
	}

	public function insertToken(string $token, string $selector, int $adminId, string $expires): bool
	{
		$query = "INSERT INTO `Token_Admin` (token, selector, admin, expires) VALUES(:token, :selector, :admin, :expires)";

		try {
			$this->pdoStatement = $this->pdo->prepare($query);

			$this->pdoStatement->bindParam('token', $token, \PDO::PARAM_STR);
			$this->pdoStatement->bindParam('selector', $selector, \PDO::PARAM_STR);
			$this->pdoStatement->bindParam('admin', $adminId, \PDO::PARAM_INT);
			$this->pdoStatement->bindParam('expires', $expires, \PDO::PARAM_STR);

			$valid = $this->pdoStatement->execute();

			return $valid;
		} catch (PDOException $e) {
			handleSqlErrors($query, $e->getMessage());
			return false;
		}
	}
}
