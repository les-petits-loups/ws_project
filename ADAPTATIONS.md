# ADAPTATIONS

## 1. Fonctionnalités 

### A. Utilisateurs

Une adaptation des règles de validation des utilisateurs (qui étaient déjà en place lors du projet PHP Back Office, mais pas utilisées) a été réalisée. Nous avons ajouté des règles et enlevé une règle déjà existante.

### B. Routing

Le système de routing a dû être légèrement adapté afin de ne pas bloquer les requêtes faites à l'API.
Nous avons également pris en compte la requête avant d'accéder à une route, dans le fichier App.php. Cela nous permet de récupérer les informations associées à cette dernière.

### C. Requêtes

Un point d'API a été modifié afin d'obtenir les réponses à une question sans pour autant savoir directement si les réponse sont vraies ou fausses (isRight). Cette modification a été effectuée afin d'empêcher l'utilisateur de tricher en regardant ce qu'il se passe dans l'onglets Networks. Deux points d'API ont également été ajoutés; un point pour récupérer une réponse (et, dans ce cas, également savoir si elle vraie ou fausse) ainsi qu'un point pour récupérer toutes les réponses, paginées.

### D. WebRTC

Le serveur de WebSockets a subi des modifications afin d'être capable d'aider à la mise en place du WebRTC. 

## 2. Architecture globale

### A. Ajout du dossier API 

Un dossier API a été rajouté dans le dossier "app", afin de stocker toutes les nouvelles classes créées pour répondre aux besoins de l'API. 

### B. Ajout de routes pour l'API

Des routes ont été rajoutées dans le fichier routing.php, démarquées du reste par des commentaires. 
