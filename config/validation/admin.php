<?php
return [
    'firstName' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le prénom ne peut pas être vide'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le prénom doit comporter entre 3 et 255 caractères'
        ]
    ],
    'lastName' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le nom de famille ne peut pas être vide'
        ],

        [
            'rule' => 'no_space',
            'message' => 'Le nom de famille ne peut pas comporter d\'espace'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le nom de famille doit comporter entre 3 et 255 caractères'
        ]
    ],
    'email' => [
        [
            'rule' => 'email',
            'message' => 'L\'email n\'est pas valide'
        ],
        [
            'rule' => 'unique_email',
            'message' => 'Cet email est déjà associé à un autre compte.'
        ]
    ],
    'password' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le mot de passe ne peut pas être vide'
        ],

        [
            'rule' => 'no_space',
            'message' => 'Le mot de passe ne peut pas comporter d\'espace'
        ],
        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le mot de passe doit comporter entre 3 et 255 caractères'
        ]
    ]
];
