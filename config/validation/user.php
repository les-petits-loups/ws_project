<?php
return [
    'firstName' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le prénom ne peut pas être vide'
        ],

        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le prénom doit comporter entre 3 et 255 caractères'
        ]
    ],
    'lastName' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le prénom ne peut pas être vide'
        ],

        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le nom de famille doit comporter entre 3 et 255 caractères'
        ]
    ],
    'email' => [
        [
            'rule' => 'email',
            'message' => 'L\'email n\'est pas valide'
        ]
    ],
    'password' => [
        [
            'rule' => 'not_blank',
            'message' => 'Le mot de passe ne peut pas être vide'
        ],

        [
            'rule' => 'length',
            'max' => 255,
            'min' => 3,
            'message' => 'Le mot de passe doit comporter entre 3 et 255 caractères'
        ]
    ],
];
