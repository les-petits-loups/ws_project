// Call the dataTables jQuery plugin
// $(document).ready(function () {
//     $("#dataTable").DataTable({
//         language: {
//             url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
//         },
//         dom: "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6 d-flex justify-content-end'<'button'>f>>",
//         "pageLength": 20,
//         "lengthMenu": [ 10, 20, 25, 50, 75, 100 ],
//         initComplete: function (settings, json) {
//             $(".row:eq(0)").addClass("mt-2 mb-2");
//             let pathname = window.location.pathname;
//             $("div.button").html(`<a type="button" style="margin-right: 10px;" class="btn btn-success d-flex" href="/..${pathname.slice(0, -1)}/new/">Créer</a>`);
//         }
//     });
// });
