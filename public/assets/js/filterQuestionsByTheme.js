let selectedTheme = $('#js-data-fetch-theme').val();
// Detect click on select theme
$('#js-data-fetch-theme').on('change', function (e) {
    e.preventDefault();
    selectedTheme = $('#js-data-fetch-theme').val();
    document.location.href = `/questions/1?theme=${selectedTheme}`
});

// Override pagination
document.querySelectorAll('.clickable-redirection').forEach(function (element) {
    if (selectedTheme > 0) {
        const pageNumber = element.dataset.page;
        element.href = `/questions/${pageNumber}?theme=${selectedTheme}`;
    }
})
