const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const filter = urlParams.get('filter');

// Override pagination
document.querySelectorAll('.clickable-redirection').forEach(function (element) {
    if (filter !== null) {
        const pageNumber = element.dataset.page;
        element.href = `/quizzs/${pageNumber}?filter=${filter}`;
    }
})