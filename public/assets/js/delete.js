document
    .querySelectorAll('.delete-object')
    .forEach(function (element) {

        const currentClass = document.getElementById("searcher").getAttribute("data-className");
        // gives back something like "admin", "question" or "theme"

            element.addEventListener('click', function (e) {
                e.preventDefault();
                const id = this.getAttribute('data-id');
                $('.toast-title').html('Suppression');

                fetch(`/../../${currentClass}/delete/${id}`, {
                    method: 'DELETE'
                }).then(() => {
                    let toastMessage;
                    switch (currentClass) {
                        case "admin":
                            toastMessage = ('La suppression de l\'administrateur s\'est bien déroulée.');
                            break;
                        case "question":
                            toastMessage = ('La suppression de la question s\'est bien déroulée.');
                            break;
                        case "theme":
                            toastMessage = ('La suppression du thème s\'est bien déroulée.');
                            break;
                        default:
                            toastMessage = ('La suppression s\'est bien déroulée.');
                    }
                    $('.toast-body-message').html(toastMessage);
                    $('.toast-success').toast('show');
                    $(`#tr${id}`).remove();
                }).catch((e) => {
                    $('.toast-body-message').html('Une erreur est survenue.');
                    $('.toast-error').toast('show');
                });
            });
        }
    );