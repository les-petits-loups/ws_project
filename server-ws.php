<?php

require_once './utils/utils.php';
require_once __DIR__ . '/vendor/autoload.php';

use App\Database\QuizzDB;
use App\Database\UserDB;
use App\Entity\Quizz;
use App\Entity\Question;
use App\Database\QuestionDB;
use App\Database\PossibleAnswerDB;
use App\Api\Quizz\QuizzDuoNode;

$rooms = [];

$server = new Hoa\Socket\Server('ws://127.0.0.1:8080');
$websocket = new Hoa\Websocket\Server($server);

$websocket->getConnection()->setNodeName(QuizzDuoNode::class);

$websocket->on('open', function (Hoa\Event\Bucket $bucket) {
    echo 'new connection', "\n";

    return;
});

$websocket->on('message', function (Hoa\Event\Bucket $bucket) {
    global $rooms;
    global $server;
    global $websocket;

    $data = $bucket->getData();
    $msgInfo = json_decode($data['message'], true);
    $node = $bucket->getSource()->getConnection()->getCurrentNode();
    
    $nodes = $server->getNodes();

    // check if the same user isn't playing more than one game! :)
    foreach($nodes as $existingNode) {
        if (($existingNode->getId() === $msgInfo['id']) && ($existingNode !== $node)) {
            $alreadyPlayingMessage = ["alreadyPlaying" => true];
            $node->setId(-1);
            $websocket->send(json_encode($alreadyPlayingMessage), $node);
            return;
        }
    }
    
    $node->setId($msgInfo['id']);
    
    if (array_key_exists("initiatorCheck", $msgInfo)) {
        $roomIndex = findRoomIndexByRoomId($msgInfo['room']);

        foreach ($rooms as &$room) {
            if ($room['user1'] === $msgInfo['id'] || $room['user2'] === $msgInfo['id']) {
                if ($room['emitterOffer'] === -1) {
                    $websocket->send(json_encode(['isInitiator' => true, 'room' => $room['id']]));
                } else {
                    $websocket->send(json_encode([
                        'isInitiator' => false,
                        'emitterOffer' => $room['emitterOffer'],
                        'room' => $room['id']
                    ]));
                }
            }
        };
    } else if (array_key_exists("offer", $msgInfo)) {
        // Only get the offer info - means that one of the users has clicked the "Activate Camera" btn
        foreach ($rooms as &$room) {
            if ($room['user1'] === $msgInfo['id'] || $room['user2'] === $msgInfo['id']) {
                if ($room['emitterOffer'] === -1) {
                    $room['emitterOffer'] = $msgInfo['offer'];

                    $otherUserID = $msgInfo['id'] === $room['user1'] ? $room['user2'] : $room['user1'];
                    
                    // If other user is in the room -> send message and ask if he wants to see user1
                    if($otherUserID !== -1) {
                        $askUser = ['askUser' => 'askUser', 'emitterOffer' => $room['emitterOffer'], 'room' => $room['id']];
                        
                        foreach($nodes as $userNode) {
                            if ($userNode->getId() === $otherUserID) {
                                $websocket->send(json_encode($askUser), $userNode);
                            } 
                        }
                    }
                } else {
                    $room['receiverOffer'] = $msgInfo['offer'];

                    foreach ($nodes as $userNode) {
                        if ($userNode->getId() === $room['user1'] || $userNode->getId() === $room['user2']) {

                            $websocket->send(json_encode([
                                'receiverOffer' => $room['receiverOffer'], 'room' => $room['id']
                            ]), $userNode);
                        }
                    }
                }
            }
        }
    } else if (array_key_exists("changingTheme", $msgInfo) && array_key_exists("room", $msgInfo)) {
        // It means that user one is changing the theme while waiting for another user. We just need to remove the old room's theme
        $roomIndex = findRoomIndexByRoomId($msgInfo['room']);

        $rooms[$roomIndex]["theme"] = -1;

    } else if (array_key_exists("theme", $msgInfo) && !array_key_exists("answer", $msgInfo) && array_key_exists("room", $msgInfo)) {
        // It means that the first user chose a theme for the room! :)
        $roomIndex = findRoomIndexByRoomId($msgInfo['room']);

        if ($rooms[$roomIndex]) {
            $rooms[$roomIndex]["theme"] = $msgInfo["theme"];

            if ($rooms[$roomIndex]["user2"] !== -1) {
                $rooms[$roomIndex]["questions"] = getFourRandomQuestionsByTheme($msgInfo["theme"]);
                sendQuestion($nodes, $roomIndex, "user1");
            }
        }

    } else if (array_key_exists('room', $msgInfo) && array_key_exists('answer', $msgInfo)) {
        // It means the user is already in a room: he is playing ;) 
        $roomIndex = findRoomIndexByRoomId($msgInfo['room']);

        if ($rooms[$roomIndex]) {
            $possibleAnswerDB = new PossibleAnswerDB();

            // Check if the answer is right
            $lastQuestionIndex = $rooms[$roomIndex]["currentQuestionIndex"] > 0 ? $rooms[$roomIndex]["currentQuestionIndex"] - 1 : $rooms[$roomIndex]["currentQuestionIndex"];
            $idQuestion = $rooms[$roomIndex]["questions"][$lastQuestionIndex];


            if ($msgInfo["answer"] !== null) {
                $answer = $possibleAnswerDB->getAnswerById($msgInfo["answer"]);
                if ($answer->getIsRight()) {
                    // The good answer comes from user1
                    if ($rooms[$roomIndex]["user1"] === $node->getId()) {
                        $rooms[$roomIndex]["user1Points"] += 1;
                    } else {
                        // The good answer comes from user2  
                        $rooms[$roomIndex]["user2Points"] += 1;
                    }
                }
            }

            // Check if the game is finished
            if ($rooms[$roomIndex]["currentQuestionIndex"] === 4) {
                sendResults($nodes, $roomIndex, $rooms[$roomIndex]["user2Points"], $rooms[$roomIndex]["user1Points"]);
            } else {
                // Send next question :)
                sendQuestion($nodes, $roomIndex, $rooms[$roomIndex]["turn"]);
            }
        }
    } else {
        // It means the user is looking for a room ;) 

        if (!empty($rooms) && count($rooms) > 0) {
            foreach ($rooms as &$room) {
                if ($room["user2"] === -1 && $room["user1"] !== $msgInfo["id"]) {
                    // It means that a room doesn't have a second player, it needs a second player
                    $room["user2"] = $msgInfo["id"];

                    // We need to send the first question (id) to the user1 if he already chose a Theme
                    if ($room["theme"] !== -1) {
                        $roomIndex = findRoomIndexByRoomId($room["id"]);
                        $room["questions"] = getFourRandomQuestionsByTheme($room["theme"]);
                        sendQuestion($nodes, $roomIndex, "user1");
                        return;
                    } else {

                        $chooseThemeMessage = ["room" => $room["id"], "chooseTheme" => false];
                        $websocket->send(json_encode($chooseThemeMessage), $node);
                        return;
                    }
                }
            }
            // It means that no one is currently looking for a second player, we need to create a new room!
            $newRoom["id"] = count($rooms);
            $newRoom["user1"] = $msgInfo["id"];
            $newRoom["user2"] = -1;
            $newRoom["questions"] = [];
            $newRoom["user1Points"] = 0;
            $newRoom["user2Points"] = 0;
            $newRoom["emitterOffer"] = -1;
            $newRoom["receiverOffer"] = -1;
            $newRoom["turn"] = "user1";
            $newRoom["currentQuestionIndex"] = 0;
            $newRoom["theme"] = -1;
            $rooms[] = $newRoom;

            $chooseThemeMessage = ["room" => $newRoom["id"],  "chooseTheme" => true];
            $websocket->send(json_encode($chooseThemeMessage), $node);
        } else {
            // It means that no one is currently looking for a second player, we need to create a new room!
            $room["id"] = count($rooms);
            $room["user1"] = $msgInfo["id"];
            $room["user2"] = -1;
            $room["questions"] = [];
            $room["user1Points"] = 0;
            $room["user2Points"] = 0;
            $room["emitterOffer"] = -1;
            $room["receiverOffer"] = -1;
            $room["turn"] = "user1";
            $room["theme"] = -1;
            $room["currentQuestionIndex"] = 0;
            $rooms[] = $room;

            $chooseThemeMessage = ["room" => $room["id"], "chooseTheme" => true];
            $websocket->send(json_encode($chooseThemeMessage), $node);
        }
    }

    return;
});

$websocket->on('close', function (Hoa\Event\Bucket $bucket) {
    global $rooms;
    global $server;

    $node = $bucket->getSource()->getConnection()->getCurrentNode();
    $nodeId = $node->getId();

    if ($nodeId !== -1) {
        $roomIndex = findRoomIndexByNodeId($nodeId);
        $nodes = $server->getNodes();

        $roomDuplicate = $rooms[$roomIndex];

        // Check if the game has started
        if ($rooms[$roomIndex]["theme"] !== -1) {
            // destroy room associated with the node. 
            array_splice($rooms, $roomIndex, 1);
            // send message to both user to say the connection has been closed. 
            sendConnectionClosed($nodes, $roomDuplicate);

        } else if ($rooms[$roomIndex]["theme"] === -1 && $rooms[$roomIndex]["user2"] === $nodeId) {
            // It means that user2 left a game that didn't even begin: there's no point 
            // in destroying the room, we just have to remove user2
            $rooms[$roomIndex]["user2"] = -1;
            $room[$roomIndex]["emitterOffer"] = -1;
            $room[$roomIndex]["receiverOffer"] = -1;
        } else if ($rooms[$roomIndex]["theme"] === -1 && $rooms[$roomIndex]["user1"] === $nodeId) {
            // It means that user1 left the game while chosing which theme he wants.
            // we need to destroy the room so that user2 knows that he will need to look for a new room.

            // destroy room associated with the node. 
            array_splice($rooms, $roomIndex, 1);

            // send message to both user to say the connection has been closed. 
            sendConnectionClosed($nodes, $roomDuplicate);
        }
        
    }

    return;
});

$websocket->run();

function getFourRandomQuestionsByTheme(int $idTheme)
{
    $questionDB = new QuestionDB();
    $randomQuestions = $questionDB->getFourRandomQuestionsByTheme($idTheme);
    return array_map('getQuestionId', $randomQuestions);
}

function getQuestionId(Question $question)
{
    return $question->getId();
}

function findRoomIndexByRoomId(int $roomId)
{
    global $rooms;
    $index = 0;

    foreach ($rooms as $room) {
        if ($room["id"] === $roomId) {
            return $index;
        }
        $index++;
    }
    return false;
}

function findRoomIndexByNodeId(int $id)
{
    global $rooms;
    $index = 0;

    foreach ($rooms as $room) {
        if (($room["user1"] === $id) || ($room["user2"] === $id)) {
            return $index;
        }
        $index++;
    }
    return false;
}

function sendQuestion($nodes, int $roomIndex, string $turn)
{
    global $rooms;
    global $websocket;

    $questionIndex = $rooms[$roomIndex]["currentQuestionIndex"];
    $message1 = ["room" => $rooms[$roomIndex]["id"], "question" => $rooms[$roomIndex]["questions"][$questionIndex], "yourTurn" => true, "finished" => false];
    $message2 = ["room" => $rooms[$roomIndex]["id"], "yourTurn" => false, "finished" => false];

    $messageUser1 = $turn === "user1" ? $message1 : $message2;
    $messageUser2 = $turn === "user1" ? $message2 : $message1;

    $messageUser1["against"] = $rooms[$roomIndex]["user2"];
    $messageUser2["against"] = $rooms[$roomIndex]["user1"];
    
    foreach($nodes as $userNode) {
        if ($userNode->getId() === $rooms[$roomIndex]["user1"]) {
            $websocket->send(json_encode($messageUser1), $userNode);
        } else if ($userNode->getId() === $rooms[$roomIndex]["user2"]) {
            $websocket->send(json_encode($messageUser2), $userNode);
        }
    }

    // after we send the question to the two users (last user to play is user2), 
    //we can pass onto the next question because the two users have received the question. 
    if ($turn === "user2") {
        $rooms[$roomIndex]["currentQuestionIndex"] += 1;
    }

    // it's the other user's turn to play now ;) 
    $rooms[$roomIndex]["turn"] = $turn === "user1" ? "user2" : "user1";
}

function sendResults($nodes, int $roomIndex, int $user2Points, int $user1Points)
{
    global $rooms;
    global $websocket;

    $isTieMatch = $user1Points === $user2Points ? true : false;
    $isUser1Winner = $user1Points > $user2Points ? true : false;

    $userDB = new UserDB();
    $quizzDB = new QuizzDB();
    $user1 = $userDB->getUserById($rooms[$roomIndex]["user1"]);
    $user2 = $userDB->getUserById($rooms[$roomIndex]["user2"]);

    if (!$isTieMatch) {
        if ($isUser1Winner) {
            $winner = $user1;
        } else {
            $winner = $user2;
        }
    } else {
        $winner = null;
    }

    $quizz = new Quizz([
        "mode" => 2,
        "user1" => $user1,
        "user2" => $user2,
        "questions" => $rooms[$roomIndex]["questions"],
        "startAt" => new \DateTime(),
        "winner" => $winner
    ]);
    $quizzDB->addQuizz($quizz);

    $user1OldPoints = $user1->getPoints();
    $user2OldPoints = $user2->getPoints();

    if ($isTieMatch) {
        // Add 5 points to each player
        $user1->setPoints($user1OldPoints + 5);
        $user2->setPoints($user2OldPoints + 5);

        $userDB->updateUser($user1);
        $userDB->updateUser($user2);
    } else if ($isUser1Winner) {
        // Add 10 points to user1
        $user1->setPoints($user1OldPoints + 10);
        $userDB->updateUser($user1);
    } else {
        // Add 10 points to user2
        $user2->setPoints($user2OldPoints + 10);
        $userDB->updateUser($user2);
    }

    $messageUser1 = [
        "room" => $rooms[$roomIndex]["id"], 
        "question" => -1, 
        "yourTurn" => true, 
        "finished" => true, 
        "winner" => isset($isTieMatch) && $isTieMatch ? "tie" : $isUser1Winner,
        "score" => $user1Points
    ];
    $messageUser2 = [
        "room" => $rooms[$roomIndex]["id"],
        "question" => -1,
        "yourTurn" => true,
        "finished" => true,
        "winner" => isset($isTieMatch) && $isTieMatch ? "tie" : !$isUser1Winner,
        "score" => $user2Points
    ];

    foreach ($nodes as $userNode) {
        if ($userNode->getId() === $rooms[$roomIndex]["user1"]) {
            $websocket->send(json_encode($messageUser1), $userNode);
        } else if ($userNode->getId() === $rooms[$roomIndex]["user2"]) {
            $websocket->send(json_encode($messageUser2), $userNode);
        }
    }
}

function sendConnectionClosed($nodes, array $roomDuplicate)
{
    global $websocket;

    $message = ["closed" => true];

    foreach ($nodes as $userNode) {
        if ($userNode->getId() === $roomDuplicate["user1"]) {
            $websocket->send(json_encode($message), $userNode);
        } else if ($userNode->getId() === $roomDuplicate["user2"]) {
            $websocket->send(json_encode($message), $userNode);
        }
    }
}